﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
    public int level;
    public Button B;
    void Start()
    {
        B = GetComponent<Button>();

        if (level > 1)
        { B.interactable = false; }

        

    }

    public void LoadScene()
    {
        SceneManager.LoadScene(level);
    }
    public void Update()
    {
        if (PlayerPrefs.HasKey("LevelUnlock") == true)
        {
            if (PlayerPrefs.GetInt("LevelUnlock") >= level)
            {
                B.interactable = true;
            }
            else
            {
                B.interactable = false;
            }
        }
    }

}
