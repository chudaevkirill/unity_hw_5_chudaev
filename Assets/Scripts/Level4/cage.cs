﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cage : MonoBehaviour
{
    public GameObject victor;
    public GameObject cubs;
    public GameObject dangerous1;
    IEnumerator trap(float time)
    {
        cubs.SetActive(true);
        yield return new WaitForSeconds(time);
        dangerous1.SetActive(true);
    }

    public void OnTriggerEnter(Collider other)
    {
        victor.SetActive(false);

        StartCoroutine(trap(0.5f));


    }
}
