﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Control : MonoBehaviour
{

    public void Start()
    {
        if (PlayerPrefs.HasKey("LevelUnlock") == false)
        {
            PlayerPrefs.SetInt("LevelUnlock", 1);
        }

    }

    public void Reset()
    {
        PlayerPrefs.SetInt("LevelUnlock", 1);
    }

}
