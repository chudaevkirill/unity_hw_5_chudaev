﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game4
{

[ExecuteAlways]
public class CameraFocus : MonoBehaviour
{
  public GameObject target;
  Vector3 velocity;
  public float smoothTime;
  new Camera camera;

  void Start()
  {
    camera = GetComponentInChildren<Camera>();
    if(target == null)
      target = GameObject.FindGameObjectWithTag("Player");
  }

  void LateUpdate()
  {
    if(target == null)
    {
      Start();
      return;
    }

    camera.transform.rotation = Quaternion.LookRotation((target.transform.position - camera.transform.position).normalized);

    if(Application.isPlaying)
      transform.position = Vector3.SmoothDamp(transform.position, target.transform.position, ref velocity, smoothTime);
    else
      transform.position = target.transform.position;
  }
}

}
