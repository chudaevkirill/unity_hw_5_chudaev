﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pit : MonoBehaviour
{

    public void Start()
    {
#if UNITY_EDITOR
        this.GetComponent<Renderer>().material.SetColor("_Color", Color.white);
#endif
    }


    public void OnTriggerEnter(Collider other)
    {
        Destroy(this.gameObject);
    }
}
