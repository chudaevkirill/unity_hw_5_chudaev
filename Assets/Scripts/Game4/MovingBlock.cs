using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game4
{

public class MovingBlock : MonoBehaviour
{
  public GameObject a;
  public GameObject b;
  Rigidbody block;
  Block block_component;

  public float speed;

  public float bounce;

  void Start()
  {
    block = GetComponentInChildren<Rigidbody>();
    block_component = block.gameObject.AddComponent<Block>();
    block_component.bounce = bounce;
  }

  void FixedUpdate()
  {
    block.transform.position = Vector3.Lerp(a.transform.position, b.transform.position, Mathf.PingPong(Time.fixedTime * speed, 1.0f));
  }

  void Update()
  {
    block_component.bounce = bounce;
    block.transform.position = Vector3.Lerp(a.transform.position, b.transform.position, Mathf.PingPong(Time.time * speed, 1.0f));
  }

  class Block : MonoBehaviour
  {
    public float bounce;

    void OnCollisionEnter(Collision other)
    {
      if(other.rigidbody == null)
        return;

      var cp = other.contacts[0];
      var dist = (other.gameObject.transform.position - cp.point).normalized;
      other.rigidbody.AddForce(dist * bounce, ForceMode.Impulse);

      var sphere = other.gameObject.GetComponent<Sphere>();
      if(sphere == null)
        return;

      sphere.Hit();
    }
  }
}

}
