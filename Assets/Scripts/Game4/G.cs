using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game4
{

public class G : MonoBehaviour
{
  public static G self;
        public int level;

  public Text victory;

  void Awake()
  {
    self = this;
  }

  void Start()
  {
    victory.gameObject.SetActive(false);
  }

  public void OnVictory(bool is_victory)
  {
    if(is_victory)
    {
      victory.gameObject.SetActive(true);
      StartCoroutine(WaitAndNext());
    }
    else
      SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
  }

  IEnumerator WaitAndNext()
  {
            if (PlayerPrefs.GetInt("LevelUnlock") <= level && level!=0)
            { PlayerPrefs.SetInt("LevelUnlock", level+1); }
            
    yield return new WaitForSeconds(1.0f);
    SceneManager.LoadScene(0);
  }
}

}
