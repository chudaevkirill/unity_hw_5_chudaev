using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game4
{

[RequireComponent(typeof(Rigidbody))]
public class FallBlock : MonoBehaviour
{
  Rigidbody self;
  Vector3 top;
  public Vector3 gravity = new Vector3(0.0f, 20.0f, 0.0f);

  Vector3 velocity;
  public float smoothTime = 4.0f;

  void Start()
  {
    self = GetComponent<Rigidbody>();
    top = self.transform.position;
  }

  void SetGravity(bool state)
  {
    self.useGravity = state;
    self.isKinematic = !state;
  }

  void FixedUpdate()
  {
    if(self.useGravity)
    {
      self.AddForce(gravity * Time.fixedDeltaTime, ForceMode.Impulse);
    }
    else
    {
      transform.position = Vector3.SmoothDamp(transform.position, top, ref velocity, smoothTime);
    }
  }

  void Fall(Collider other)
  {
    if(other.gameObject.GetComponent<Sphere>() == null)
      return;

    if((top - transform.position).magnitude < 0.01f)
    {
      SetGravity(true);
    }
  }

  void OnTriggerEnter(Collider other)
  {
    Fall(other);
  }

  void OnTriggerStay(Collider other)
  {
    Fall(other);
  }

  void Update()
  {
    if(!self.useGravity)
      transform.position = Vector3.SmoothDamp(transform.position, top, ref velocity, smoothTime);
  }

  void OnCollisionEnter(Collision other)
  {
    SetGravity(false);

    var sphere = other.gameObject.GetComponent<Sphere>();
    if(sphere == null)
      return;

    sphere.Hit();
  }
}

}
