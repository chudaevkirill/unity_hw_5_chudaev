﻿using Game4;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    public float bounce;
    public void OnCollisionEnter(Collision other)
    {
        if (other.rigidbody == null)
            return;

        var cp = other.contacts[0];
        var dist = (other.gameObject.transform.position - cp.point).normalized;
        other.rigidbody.AddForce(dist * bounce, ForceMode.Impulse);

        var sphere = other.gameObject.GetComponent<Sphere>();
        if (sphere == null)
            return;

    }
}
